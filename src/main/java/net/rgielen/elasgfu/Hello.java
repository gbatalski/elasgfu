package net.rgielen.elasgfu;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class Hello {

    public static void main(String[] args) {
        System.out.println(new HelloProvider("Hello!").getHelloVal());
    }
}
