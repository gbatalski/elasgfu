package net.rgielen.elasgfu.todo.repository;

import net.rgielen.elasgfu.todo.entity.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface TodoItemRepository extends JpaRepository<TodoItem, Integer> {
}
